public class VoucherCodeOutput {

    public static void printGeneratedCode(String code, String expirationDate) {
        System.out.println("  [Generated Code]   " + code + "   Expires on: " + expirationDate);
    }

    public static void printRandomCodesHeader() {
        System.out.println("-----------------------------------------------------------------------");
        System.out.println("                         Random 10 Generated Codes                    ");
        System.out.println("-----------------------------------------------------------------------");
    }

    public static void printRandomCode(String code, String expirationDate) {
        System.out.println("_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ ");
        System.out.println("[Random Code]      " );
        System.out.println(code);
        System.out.println("[Expires on ] : "  + expirationDate);
        System.out.println("_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ ");
    }

    public static void printSavedCodes() {
        System.out.println("-----------------------------------------------------------------------");
        System.out.println("                 Saved Generated Codes to XML File                    ");
        System.out.println("-----------------------------------------------------------------------");
    }
}
