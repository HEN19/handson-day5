import java.util.Date;
public class VoucherCode {
    private String code;
    private Date expirationDate;

    public VoucherCode(String code, Date expirationDate) {
        this.code = code;
        this.expirationDate = expirationDate;
    }

    public String getCode() {
        return code;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public boolean isExpired() {
        return System.currentTimeMillis() > expirationDate.getTime();
    }
}