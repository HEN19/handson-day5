import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import java.util.Random;
import java.util.UUID;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class VoucherCodeGenerator {
    private static final int CODE_LENGTH = 10;
    private static final int NUMBER_OF_CODES_TO_GENERATE = 10;
    private static final Logger logger = Logger.getLogger(VoucherCodeGenerator.class.getName());

    public static void main(String[] args) {
        try {
            FileHandler fileHandler = new FileHandler("voucherCodes.log");
            fileHandler.setFormatter(new SimpleFormatter());
            logger.addHandler(fileHandler);
            logger.setLevel(Level.INFO);
        } catch (Exception e) {
            e.printStackTrace();
        }

        // menampung voucher yang dipanggil
        List<VoucherCode> voucherCodes = loadCodesFromXml(); 

        // Mengenerate code
        for (int i = 0; i < NUMBER_OF_CODES_TO_GENERATE; i++) {
            VoucherCode code = generateUniqueCode(voucherCodes);
            voucherCodes.add(code);
            VoucherCodeOutput.printGeneratedCode(code.getCode(),code.getExpirationDate().toString());
            logger.info("Generated code: " + code.getCode() + " expires on " + code.getExpirationDate());
        }

        // menghapus code yang expired
        cleanupExpiredCodes(voucherCodes);

        VoucherCodeOutput.printSavedCodes();

        //update xml
        saveCodesToXml(voucherCodes); 

        // printout random
        List<VoucherCode> randomCodes = selectRandomCodes(voucherCodes, NUMBER_OF_CODES_TO_GENERATE);
        VoucherCodeOutput.printRandomCodesHeader();;
        for (VoucherCode code : randomCodes) {
            VoucherCodeOutput.printRandomCode(code.getCode(), code.getExpirationDate().toString());
            logger.info("Random code: " + code.getCode() + " expires on " + code.getExpirationDate());
        }
    }

    // method untuk menampung sementara
    private static List<VoucherCode> loadCodesFromXml() {
        return new ArrayList<>();
    }

    private static void saveCodesToXml(List<VoucherCode> codes) {
        logger.info("Saved codes to XML");
    }

    private static void cleanupExpiredCodes(List<VoucherCode> codes) {
        // menghapus code
        codes.removeIf(VoucherCode::isExpired);
    }

    private static VoucherCode generateUniqueCode(List<VoucherCode> existingCodes) {
        String code;
        Date expirationDate;

        // Generate random
        code = UUID.randomUUID().toString().replaceAll("[^a-zA-Z0-9]", "").substring(0, CODE_LENGTH);

        // set expired 7 hari setelah di generate
        long expirationTimeMillis = System.currentTimeMillis() + (7 * 24 * 60 * 60 * 1000);
        expirationDate = new Date(expirationTimeMillis);

        VoucherCode voucherCode = new VoucherCode(code, expirationDate);

        // validasi voucher
        for (VoucherCode existingCode : existingCodes) {
            if (existingCode.getCode().equals(code) && !existingCode.isExpired()) {
                // jika tidak unik dan belum expired, generate code
                return generateUniqueCode(existingCodes);
            }
        }

        return voucherCode;
    }

    private static List<VoucherCode> selectRandomCodes(List<VoucherCode> codes, int count) {
        List<VoucherCode> randomCodes = new ArrayList<>();
        Random random = new Random();

        for (int i = 0; i < count; i++) {
            int randomIndex = random.nextInt(codes.size());
            randomCodes.add(codes.get(randomIndex));
        }

        return randomCodes;
    }
}


